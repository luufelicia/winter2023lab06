public class Jackpot
{
	public static void main (String[] args)
	{
		System.out.println("Hello, welcome to Jackpot!");
		Board gameBoard = new Board(); 
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		while (gameOver!=true)
		{
			System.out.println(gameBoard);
			if (gameBoard.playATurn() == true)
			{
				gameOver = true;
			}
			
			if (gameBoard.playATurn() == false)
			{
				numOfTilesClosed = 1;
			}
		}
		
		if (numOfTilesClosed >= 7)
		{
			System.out.println("You reached the Jackpot and won! Congratulations!");
		}
		
		else
		{
			System.out.println("Unfortunately, you lost. Better luck next time!");
		}
	}
	
}
import java.util.Random;
public class Die
{
	private int faceValue;
	private Random randoNum;
	
	public Die()
	{
		this.faceValue = 1;
		this.randoNum = new Random();
	}
	
	public int getFaceValue()
	{
		return this.faceValue;
	}
	
	public int roll()
	{
		return this.faceValue = (randoNum.nextInt(6))+1;
	}
	
	public String toString()
	{
		return "Face value: " + this.faceValue;
	}
	



}
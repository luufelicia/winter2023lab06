public class Board
{
	private Die dice1;
	private Die dice2;
	private boolean[] tiles;
	
	
	public Board()
	{
		this.dice1 = new Die();
		this. dice2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString() //if true, it's empty. if false, it contains a number
	{
		String tilesString = "";
		for (int i = 0; i<tiles.length; i++)
		{
			if (tiles[i] == false)
			{
				tilesString+= (i+1); //if false, number still the same
			}
			
			else
			{
				tilesString+= "X"; //if true, turnt over (letter faces up"
			}
			tilesString+= " ";
		}
		
		return tilesString;
	}
	
	public boolean playATurn()
	{
		int dice1Roll = dice1.roll();
		int dice2Roll = dice2.roll();
		System.out.println(dice1);
		System.out.println(dice2);

		int sumOfDice = dice1Roll+dice2Roll;
		
		if (tiles[sumOfDice-1] == false)
		{
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		
		else if (tiles[dice1Roll-1] == false)
		{
			tiles[dice1Roll-1] = true;
			System.out.println("Closing tile with the same value as die one: " + dice1Roll);
			return false;
		}
		
		else if (tiles[dice2Roll-1] == false)
		{
			tiles[dice2Roll-1] = true;
			System.out.println("Closing tile with the same value as die two: " + dice2Roll);
			return false;
		}
		
		else
		{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
	
}
	
	
	
	
